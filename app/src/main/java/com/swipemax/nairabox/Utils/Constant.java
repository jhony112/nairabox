package com.swipemax.nairabox.Utils;

/**
 * Created by Jhony112 on 2016/08/24.
 */
public class Constant {
    public static final String TAG_APP_NAME = "nairabox";
    public static final String TERMINAL_ID = "terminal_id";
    public static final String FIRST_RUN ="run_first" ;
    public static  String MERCHANT_NAME ="merchant_name" ;
    public static String MY_PREF="nairabox_pref";
    public static String LOGIN_URL="https://revision.nairabox.com/v1/cinema/index.php";
    public static String DISPLAY_PHOTO="display_photo";
}
