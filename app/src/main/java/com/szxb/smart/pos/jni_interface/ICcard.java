package com.szxb.smart.pos.jni_interface;

import android.util.Log;

public class ICcard extends halTrans{
	static {
		try {
			System.loadLibrary("ContactCard");
		} catch (Throwable e) {
			Log.e("jni", "i can't find ContactCard so!");
			e.printStackTrace();
		}
	}
	
	public static native int ICcardOpen();
	public static native int ICcardClose();
	public static native String CpuCardPowerOn(byte slot);
	public static native int CpuCardPowerOff(byte slot);
	public static native String[] CpuCardSendAPDU(byte slot,String sendData);
	
	
}
