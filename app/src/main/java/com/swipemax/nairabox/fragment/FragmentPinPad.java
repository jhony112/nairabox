package com.swipemax.nairabox.fragment;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.swipemax.nairabox.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentPinPad.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentPinPad#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentPinPad extends DialogFragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    RelativeLayout btn_one, btn_two, btn_three, btn_four, btn_five, btn_six, btn_seven, btn_eight, btn_nine, btn_zero, btn_delete;
    ImageView cicrle_1, circle_2, circle_3, circle_4;
    private OnPinFinished mListener;
    private Dialog d;
    View _view;

    public FragmentPinPad() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentPinPad.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentPinPad newInstance(String param1, String param2) {
        FragmentPinPad fragment = new FragmentPinPad();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        d = getDialog();
        if (d != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            d.getWindow().setLayout(width, height);
            // d.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.MY_PINPAD);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        _view = inflater.inflate(R.layout.fragment_pin_pad, container, false);
        setupButtons(_view);
        return _view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    void setupButtons(View v) {
        btn_one = (RelativeLayout) v.findViewById(R.id.btn_one);
        btn_one.setOnClickListener(this);
        btn_two = (RelativeLayout) v.findViewById(R.id.btn_two);
        btn_two.setOnClickListener(this);
        btn_three = (RelativeLayout) v.findViewById(R.id.btn_three);
        btn_three.setOnClickListener(this);
        btn_four = (RelativeLayout) v.findViewById(R.id.btn_four);
        btn_four.setOnClickListener(this);
        btn_five = (RelativeLayout) v.findViewById(R.id.btn_five);
        btn_five.setOnClickListener(this);
        btn_six = (RelativeLayout) v.findViewById(R.id.btn_six);
        btn_six.setOnClickListener(this);
        btn_seven = (RelativeLayout) v.findViewById(R.id.btn_seven);
        btn_seven.setOnClickListener(this);
        btn_eight = (RelativeLayout) v.findViewById(R.id.btn_eight);
        btn_eight.setOnClickListener(this);
        btn_nine = (RelativeLayout) v.findViewById(R.id.btn_nine);
        btn_nine.setOnClickListener(this);
        btn_zero = (RelativeLayout) v.findViewById(R.id.btn_zero);
        btn_zero.setOnClickListener(this);
        btn_delete = (RelativeLayout) v.findViewById(R.id.btn_delete);
        btn_delete.setOnClickListener(this);
        cicrle_1 = (ImageView) v.findViewById(R.id.circle_one);
        circle_2 = (ImageView) v.findViewById(R.id.circle_two);
        circle_3 = (ImageView) v.findViewById(R.id.circle_three);
        circle_4 = (ImageView) v.findViewById(R.id.circle_four);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnPinFinished) {
            mListener = (OnPinFinished) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnPhoneFragment");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    String PinEntered = "";


    void ChangeCircle(int number, int index, boolean reset) {

        if (!reset) {
            if (index == 0) {
                cicrle_1.setImageResource(R.drawable.active_number);
                PinEntered += number;
            } else if (index == 1) {
                circle_2.setImageResource(R.drawable.active_number);
                PinEntered += number;
            } else if (index == 2) {
                circle_3.setImageResource(R.drawable.active_number);
                PinEntered += number;
            } else if (index == 3) {
                circle_4.setImageResource(R.drawable.active_number);
                PinEntered += number;
                mListener.OnPinTyped(PinEntered);

            }
        } else {
            if (index == 1) {
                cicrle_1.setImageResource(R.drawable.idle_number);
                PinEntered = "";
            } else if (index == 2) {
                circle_2.setImageResource(R.drawable.idle_number);
                PinEntered = PinEntered.substring(0, 1);
            } else if (index == 3) {
                circle_3.setImageResource(R.drawable.idle_number);
                PinEntered = PinEntered.substring(0, 2);
            } else if (index == 4) {
                circle_4.setImageResource(R.drawable.idle_number);
                PinEntered = PinEntered.substring(0, 3);
            }

        }

    }

    private int number_count = 0;

    void increaseNumber() {
        if (number_count > 3) {
            number_count = 4;
        } else {
            number_count++;
        }
    }

    void decreaseNumber() {
        //  Toast.makeText(getActivity(), "Count "+number_count, Toast.LENGTH_SHORT).show();

        if (number_count <= 0) {
            number_count = 0;
        } else {
            number_count--;
        }
    }

    @Override
    public void onClick(View view) {
        // Toast.makeText(getActivity(), "count "+ number_count, Toast.LENGTH_SHORT).show();
        //number_count=PinEntered.length();
        switch (view.getId()) {
            case R.id.btn_one:
                ChangeCircle(1, number_count, false);
                increaseNumber();
                break;
            case R.id.btn_two:
                ChangeCircle(2, number_count, false);
                increaseNumber();
                break;
            case R.id.btn_three:
                ChangeCircle(3, number_count, false);
                increaseNumber();
                break;
            case R.id.btn_four:
                ChangeCircle(4, number_count, false);
                increaseNumber();
                break;
            case R.id.btn_five:

                ChangeCircle(5, number_count, false);
                increaseNumber();
                break;
            case R.id.btn_six:
                ChangeCircle(6, number_count, false);
                increaseNumber();
                break;
            case R.id.btn_seven:
                ChangeCircle(7, number_count, false);
                increaseNumber();
                break;
            case R.id.btn_eight:
                ChangeCircle(8, number_count, false);
                increaseNumber();
                break;
            case R.id.btn_nine:
                ChangeCircle(9, number_count, false);
                increaseNumber();
                break;
            case R.id.btn_zero:
                ChangeCircle(0, number_count, false);
                increaseNumber();
                break;
            case R.id.btn_delete:
                ChangeCircle(11, number_count, true);
                decreaseNumber();
                break;
        }

    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnPinFinished {
        // TODO: Update argument type and name
        void OnPinTyped(String pin);

    }

   public void close() {
        dismiss();
    }
}
