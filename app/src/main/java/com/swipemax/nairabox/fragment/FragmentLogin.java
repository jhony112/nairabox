package com.swipemax.nairabox.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.swipemax.nairabox.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentLogin.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class FragmentLogin extends Fragment {

    private FragmemtLoginListener mListener;
    private Button btn_login;
    private EditText merchant_ID;
    private EditText merchant_Pin;

    public FragmentLogin() {
        // Required empty public constructor
    }

    View _root;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        _root = inflater.inflate(R.layout.fragment_login, container, false);
        btn_login = (Button) _root.findViewById(R.id.btn_login);
        merchant_ID = (EditText) _root.findViewById(R.id.input_merchantID);
        merchant_Pin = (EditText) _root.findViewById(R.id.input_merchantPin);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String m_id = merchant_ID.getText().toString().trim();
                String m_pin = merchant_Pin.getText().toString().trim();
                if (m_id.length() > 0 && m_pin.length() > 0) {
                    if (mListener != null) {
                        mListener.onLogin(m_id, m_pin);
                    }

                } else {
                    Toast.makeText(getActivity(), "Please fill all fields", Toast.LENGTH_SHORT).show();
                }

            }
        });
        return _root;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmemtLoginListener) {
            mListener = (FragmemtLoginListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnPhoneFragment");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface FragmemtLoginListener {
        // TODO: Update argument type and name
        void onLogin(String id, String password);
    }
}
