package com.szxb.smart.pos.jni_interface;

import android.util.Log;

public class PinPad extends halTrans{
	
	static {
		try {
			System.loadLibrary("PinPad");
        } catch (Throwable e) {
            Log.e("jni", "i can't find PinPad so!");
            e.printStackTrace();
        }
	}
	
	public native static int PinpadOpen();
	
	public native static int PinpadClose();
	
	public native static String PinPadTrans(String sendData,int timeOut);
}
