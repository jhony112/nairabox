package com.swipemax.nairabox.activity;


import android.app.Fragment;
import android.os.Build;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.app.Activity;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.swipemax.nairabox.AppController;
import com.swipemax.nairabox.PrintString;
import com.swipemax.nairabox.R;
import com.swipemax.nairabox.Utils.Constant;
import com.swipemax.nairabox.Utils.Ticket;
import com.swipemax.nairabox.asynctasks.IssueTicket;
import com.swipemax.nairabox.fragment.FragmentPhone;
import com.swipemax.nairabox.fragment.FragmentPinPad;
import com.swipemax.nairabox.fragment.FragmentScanner;
import com.swipemax.nairabox.fragment.Fragment_Details;

import java.io.IOException;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class BaseActivity extends FragmentActivity implements View.OnClickListener,
        FragmentScanner.FragmentScannerListener, FragmentPinPad.OnPinFinished,FragmentPhone.OnPhoneFragment,
        Fragment_Details.OnFragmentDetailListener{
    ImageView profile_pic, img_barCode, img_dialPad;
    private TextView txt_name;
    private TextView txt_id;
    private Animation pulse;
    private Handler handler;
    private Animation pulse2;
    private ImageView img_help;
    private Animation pulse3;
    private RelativeLayout overlay, main_layout;
    private RelativeLayout baba_nla;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    void setWindow() {
        final Window win = getWindow();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        win.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        win.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
        String photo_url = getIntent().getStringExtra(Constant.DISPLAY_PHOTO);
        String term_id = getIntent().getStringExtra(Constant.TERMINAL_ID);
        String name = getIntent().getStringExtra(Constant.MERCHANT_NAME);
        setContentView(R.layout.activity_base);
        overlay = (RelativeLayout) findViewById(R.id.overlay);
        baba_nla = (RelativeLayout) findViewById(R.id.main_layout);

        main_layout = (RelativeLayout) findViewById(R.id.main_container);
        txt_name = (TextView) findViewById(R.id.merchant_name);
        profile_pic = (ImageView) findViewById(R.id.image_merchant);
        img_barCode = (ImageView) findViewById(R.id.image_qr);
        img_barCode.setOnClickListener(this);
        img_dialPad = (ImageView) findViewById(R.id.image_phone);
        img_dialPad.setOnClickListener(this);
        img_help = (ImageView) findViewById(R.id.image_help);
        img_help.setOnClickListener(this);
        LoadImage(photo_url, name);
        pulse = AnimationUtils.loadAnimation(this, R.anim.pulse);
        pulse2 = AnimationUtils.loadAnimation(this, R.anim.pulse);
        pulse3 = AnimationUtils.loadAnimation(this, R.anim.pulse);
        pulse.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                img_dialPad.startAnimation(pulse2);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        pulse2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                img_help.startAnimation(pulse3);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        handler = new Handler();
        handler.postDelayed(runnable, 10000);
        KillStatusBar();
    }
    private void  KillStatusBar()
    {
        Process proc = null;

        String ProcID = "79"; //HONEYCOMB AND OLDER

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH){
            ProcID = "42"; //ICS AND NEWER
        }

        try {
            proc = Runtime
                    .getRuntime()
                    .exec(new String[] { "su", "-c",
                            "service call activity "+ProcID+" s16 com.android.systemui" });
        } catch (IOException e) {
            Log.w("TAG","Failed to kill task bar (1).");
            e.printStackTrace();
        }
        try {
            proc.waitFor();
        } catch (InterruptedException e) {
            Log.w("Taskbar","Failed to kill task bar (2).");
            e.printStackTrace();
        }

    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
      /* do what you need to do */
            img_barCode.startAnimation(pulse);
            //  SystemClock.sleep(1000);

      /* and here comes the "trick" */
            handler.postDelayed(this, 10000);
        }
    };

    void LoadImage(String url, String name) {
        Picasso.with(this)
                .load(url)
                .placeholder(R.drawable.logo_n_only)
                .error(R.drawable.logo_n_only)
                .into(profile_pic);
        txt_name.setText(name);


        // txt_id.setText(id);
    }

    @Override
    public void onBackPressed() {
     FragmentScanner scanner= (FragmentScanner) getSupportFragmentManager().findFragmentByTag("SCANNER");
        if (scanner != null) {
            scanner.dismiss();
        }
        FragmentPhone phone= ( FragmentPhone) getSupportFragmentManager().findFragmentByTag("PHONE");
        if (phone != null) {
            phone.dismiss();
        }
        super.onBackPressed();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.image_qr:
//                FragmentPinPad pinPad= new FragmentPinPad();
//                showFragment(pinPad,"PIN_PAD");
                FragmentScanner scanner = new FragmentScanner();
                showFragment(scanner, "SCANNER");
                break;
            case R.id.image_phone:
                FragmentPhone phone = new FragmentPhone();
                showFragment(phone, "PHONE");
                break;
            case R.id.image_help:
                break;
        }
    }


    void showFragment(DialogFragment fragment, String tag) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_up, R.anim.slide_down);
        fragment.show(ft, tag);
    }

    @Override
//    Gets the scanner result from the scanner Fragment returns ticket id
    public void OnScanComplete(String Result) {
        enableViews(main_layout, false);
        Toast.makeText(BaseActivity.this, "Gimme this Scan" + Result, Toast.LENGTH_SHORT).show();
        FragmentScanner scanner= (FragmentScanner) getSupportFragmentManager().findFragmentByTag("SCANNER");
        if (scanner != null) {
            scanner.dismiss();
        }
        //run network call here and enable main layout after completion
        // Todo: My custom code addition
        IssueTicket issueTicket = new IssueTicket(this, getApplicationContext());
        issueTicket.execute(Result);
        enableViews(main_layout, true);

    }

    @Override
    public void OnScanCancelled() {

    }

    @Override
    public void OnPinTyped(String pin) {
        enableViews(main_layout, false);
        Toast.makeText(BaseActivity.this, "Gimme this pin" + pin, Toast.LENGTH_SHORT).show();
        FragmentPinPad pinpad = (FragmentPinPad) getSupportFragmentManager().findFragmentByTag("PIN_PAD");
        if (pinpad != null) {
            pinpad.close();
        }
        enableViews(main_layout, true);
    }

    private void enableViews(View v, boolean enabled) {
        if (v instanceof ViewGroup) {
            ViewGroup vg = (ViewGroup) v;
            for (int i = 0; i < vg.getChildCount(); i++) {
                enableViews(vg.getChildAt(i), enabled);
            }
        }
        v.setEnabled(enabled);
    }

    @Override
    //Redeem by phone number Callback returns Phone number entered
    public void onPhoneEntered(String phone) {
        enableViews(main_layout, false);
        Toast.makeText(BaseActivity.this, phone+"  oshey", Toast.LENGTH_SHORT).show();

        //handle phone number here
        enableViews(main_layout, true);
    }

    @Override
    public void onTicketPrint(Ticket ticket){
        PrintString p2 = new PrintString(getApplicationContext());
        p2.printTicket(ticket, AppController.getInstance().getTicketList());
    }
}
