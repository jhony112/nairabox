package com.swipemax.nairabox.Utils;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by samwul on 25/08/2016.
 */
public class Ticket implements Parcelable {
    private String id;
    private String cinema;
    private String movie;
    private ArrayList<TicketClass> classes;
    private String date;
    private String time;
    private int totalPaid;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCinema() {
        return cinema;
    }

    public void setCinema(String cinema) {
        this.cinema = cinema;
    }

    public String getMovie() {
        return movie;
    }

    public void setMovie(String movie) {
        this.movie = movie;
    }

    public ArrayList<TicketClass> getClasses() {
        return classes;
    }

    public void setClasses(ArrayList<TicketClass> classes) {
        this.classes = classes;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getTotalPaid() {
        return totalPaid;
    }

    public void setTotalPaid(int totalPaid) {
        this.totalPaid = totalPaid;
    }

    public int totalQuantity(){
        if(classes.isEmpty()){ return 0; }
        else{
            int total = 0;
            for(int i = 0; i < classes.size(); i++){
                TicketClass tclass = classes.get(i);
                total += tclass.getQuantity();
            }
            return total;
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.getId());
        out.writeString(this.getCinema());
        out.writeString(this.getMovie());
        out.writeTypedList(this.getClasses());
        out.writeString(this.getDate());
        out.writeString(this.getTime());
        out.writeInt(this.getTotalPaid());
    }

    public static final Parcelable.Creator<Ticket> CREATOR
            = new Parcelable.Creator<Ticket>() {
        public Ticket createFromParcel(Parcel in) {
            return new Ticket(in);
        }

        public Ticket[] newArray(int size) {
            return new Ticket[size];
        }
    };

    private Ticket(Parcel in) {
        this.setId(in.readString());
        this.setCinema(in.readString());
        this.setMovie(in.readString());
        in.readTypedList(classes, TicketClass.CREATOR);
        this.setDate(in.readString());
        this.setTime(in.readString());
        this.setTotalPaid(in.readInt());
    }

    public Ticket(){

    }

    @Override
    public String toString() {
        return this.getMovie();
    }
}
