package com.swipemax.nairabox;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.widget.Toast;

import com.swipemax.nairabox.Utils.Constant;
import com.swipemax.nairabox.Utils.Ticket;
import com.swipemax.nairabox.Utils.TicketClass;
import com.szxb.smart.pos.jni_interface.printer;
//打印String类型的字符串


public class PrintString {
    public static final int BIT_WIDTH = 384;

    private static final int WIDTH = 48;
    private static final int DOT_LINE_LIMIT = 200;
    private static final int DC2V_HEAD = 4;
    private static final int GSV_HEAD = 8;
    Context context;

    public PrintString(Context ctx) {
        this.context = ctx;
    }

    public String GetString(int ID) {
        String response;
        response = this.context.getResources().getString(ID);

            return response;

    }

    public String GetPref(String tag) {
        SharedPreferences sharedpreferences = context.getSharedPreferences(Constant.MY_PREF, Context.MODE_PRIVATE);
        String restoredText = sharedpreferences.getString(tag, null);
        if (restoredText != null) {
            return "null";
        } else {
            return restoredText;
        }
    }

    void printours1() {
        PrintString p = new PrintString(context);
        int i = printer.PrinterOpen();
        if (i >= 0) {

            p.printString("单位：小兵智能");
            p.bold(1);
            p.printString("职位：");

            p.inverse(1);
            p.printString("地址：深圳市科技园");

            p.center(1);
            p.printString("主营：pos");

            p.width(1);
            p.printString("终端号：2233");

            p.underline(1);
            p.printString("终端号：2233");

            p.printString("终端号：2233");

            p.printString("终端号：2233");
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss ");
            Date curDate = new Date(System.currentTimeMillis());
            //获取当前时间
            String str = formatter.format(curDate);
            p.printString("时间：" + str);
            printer.PrinterClose();
        }
    }

    void printours() {
        PrintString p = new PrintString(context);
        int i = printer.PrinterOpen();
        if (i >= 0) {
            p.center(0);
            p.printString(GetString(R.string.app_name));
            p.bold(1);
            p.printString("FilmHouse Cinema Surulere Lagos");
            p.bold(0);
            p.inverse(1);
            p.printString("Suicide Squad");
            p.inverse(0);
            p.center(1);
            p.printString("Price：N500");
            p.center(0);
            p.width(0);
            p.printString("Ticket Class ：Student");
            p.width(0);
            p.underline(1);
            p.printString("Ticket valid for movie duration ");
            p.underline(0);
         p.center(1);
            p.printString("..do it quicker");
            p.underline(1);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy/mm/dd HH:mm:ss ");
            Date curDate = new Date(System.currentTimeMillis());
            //获取当前时间
            String str = formatter.format(curDate);
            p.printString("Time ：" + str);
            printer.PrinterClose();
        }
        else {
            Toast.makeText(context, "Error printing", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean hasPaper(){

        int s = printer.PrinterStatus();
        if(s == 0){
            return false;
        }
        else
        {
            return true;
        }

    }

    public int printTicket(Ticket ticket, ArrayList<Ticket> ticketArrayList){
        PrintString p = new PrintString(context);
        int i = printer.PrinterOpen();
        boolean error = false;

        for (TicketClass tclass: ticket.getClasses()) {
            int qty = tclass.getQuantity();
            for(int x = 0; x < qty; x++){
                if(i >= 0){
                    p.center(0);
                    p.printString(GetString(R.string.app_name)); // nairabox
                    p.bold(1);
                    p.printString(ticket.getCinema()); // cinema name
                    p.bold(0);
                    p.inverse(1);
                    p.printString(ticket.getMovie()); // movie title
                    p.inverse(0);
                    p.center(1);
                    p.printString("Ticket Class: " + tclass.gettClass());
                    p.center(0);
                    p.width(0);
                    p.printString("Price: " + Integer.toString(tclass.getPrice()));
                    p.width(0);
                    p.underline(1);
                    p.printString("Ticket valid for movie duration ");
                    p.printString(" ");
                    p.printString(" ");
                    p.printString("do it quicker.. ");
                    p.printString("----------X-----------");
                    p.printString(" ");
                    p.underline(0);
                }else{
                    error = true;
                }
            }
        }

        if(error != true){
            ticketArrayList.remove(ticket);
        }
        return 1;
    }


    @SuppressWarnings("unused")
    void printString(String data) {

        data = sb(data);
        final byte[] b = StringByte.HexString2Bytes(data);
        printer.PrinterWrite(b);
        printer.PrinterWrite(printer.getCmdLf());
    }

    @SuppressWarnings("unused")
    //加粗  i=1加粗  i=0不加粗
    private void bold(int i) {
        printer.PrinterWrite(printer.getCmdEscEN(i));

    }

    //居中 1中 0右 2左
    @SuppressWarnings("unused")
    private void center(int i) {
        printer.PrinterWrite(printer.getCmdEscAN(i));
    }

    @SuppressWarnings("unused")
    //加宽
    private void width(int i) {
        switch (i) {
            case 1:
                printer.PrinterWrite(printer.getCmdEscSo());//加粗
                break;

            case 0:
                printer.PrinterWrite(printer.getCmdEscDc4());//清除加粗
                break;
        }

    }

    //倒打 i=1 true, i=0 clear
    @SuppressWarnings("unused")
    private void reverse(int i) {
        printer.PrinterWrite(printer.getCmdEsc__N(i));

    }

    //反白 i=1 反白    i=0 clear
    @SuppressWarnings("unused")
    private void inverse(int i) {
        printer.PrinterWrite(printer.getCmdGsBN(i));
    }

    //下划线  i=0~2
    @SuppressWarnings("unused")
    private void underline(int i) {
        printer.PrinterWrite(printer.getCmdEsc___N(i));

    }


    private static String sb(String content) {
        String str = content;

        String hexString = "0123456789ABCDEF";
        byte[] bytes;
        try {
            bytes = str.getBytes("GBK");// 如果此处不加编码转化，得到的结果就不是理想的结果，中文转码
            StringBuilder sb = new StringBuilder(bytes.length * 2);

            for (int i = 0; i < bytes.length; i++) {
                sb.append(hexString.charAt((bytes[i] & 0xf0) >> 4));
                sb.append(hexString.charAt((bytes[i] & 0x0f) >> 0));
                // sb.append("");
            }
            str = sb.toString();

        } catch (UnsupportedEncodingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return str;

    }


    @SuppressWarnings("unused")
    public void bitmap(Bitmap b, int left, int top) {
        byte[] result = generateBitmapArrayGSV_MSB(b, left, top);
        printer.PrinterOpen();
        printer.PrinterProbe();
        printer.PrinterStatus();
        int lines = ((result.length - GSV_HEAD) / WIDTH) - 30;
        System.arraycopy(new byte[]{0x12, 0x2A, (byte) (lines & 0xff), (byte) ((lines >> 8) & 0xff)}, 0, result, 0, GSV_HEAD);
        printer.PrinterWrite(result);
        printer.PrinterWrite(printer.getCmdLf());
        printer.PrinterClose();
    }


    private static byte[] generateBitmapArrayGSV_MSB(Bitmap bm, int bitMarginLeft, int bitMarginTop) {
        byte[] result = null;
        int n = bm.getHeight() + bitMarginTop;
        int offset = GSV_HEAD;
        result = new byte[n * WIDTH + offset];
        for (int y = 0; y < bm.getHeight(); y++) {
            for (int x = 0; x < bm.getWidth(); x++) {
                if (x + bitMarginLeft < BIT_WIDTH) {
                    int color = bm.getPixel(x, y);
                    int alpha = Color.alpha(color);
                    int red = Color.red(color);
                    int green = Color.green(color);
                    int blue = Color.blue(color);
                    if (red < 128) {
                        int bitX = bitMarginLeft + x;
                        int byteX = bitX / 8;
                        int byteY = y + bitMarginTop;
                        result[offset + byteY * WIDTH + byteX] |= (0x80 >> (bitX - byteX * 8));
                    }
                } else {
                    // ignore the rest data of this line
                    break;
                }
            }
        }
        return result;
    }

}
