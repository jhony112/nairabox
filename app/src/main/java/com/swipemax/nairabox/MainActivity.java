package com.swipemax.nairabox;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;

import com.szxb.smart.pos.jni_interface.ICcard;
import com.szxb.smart.pos.jni_interface.MSR;

import com.szxb.smart.pos.jni_interface.contactlessCard;
import com.szxb.smart.pos.jni_interface.halTrans;
import com.szxb.smart.pos.jni_interface.printer;
import com.szxb.smart.pos.jni_interface.psam;

import com.wizarpos.barcode.scanner.IScanEvent;
import com.wizarpos.barcode.scanner.ScannerRelativeLayout;
import com.wizarpos.barcode.scanner.ScannerResult;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.opengl.Visibility;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements View.OnClickListener {
	public String scanResultMy;
	private Button BtICcard;
	private static TextView showTextView;
	private EditText Print;
	private Button BtMsr;
	private Button BtRfid;
	private Button BtPrinter, MySelf, Wenzi, Code;
	private Button Btpsam;
	private Button Btclean;
	private Button gprs;
	private Button saomiao;
	public String stringResult = "";
	public IScanEvent scanLisenter;
	Handler handler;
	public static FrameLayout surfaceLayout;
	public static ScannerRelativeLayout scanView;
	static final String TAG = MainActivity.class.getSimpleName();   

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
//		PrintString p1 = new PrintString();
//		p1.printString("深圳小兵");
//		p1.printString("深圳小兵1");
//		printer.PrinterWrite(printer.getCmdEscSo());
//		p1.printString("深圳小兵");
//
//		p1.printString("深圳小兵1");
//		printer.PrinterClose();
		surfaceLayout = (FrameLayout) findViewById(R.id.surfaceViewLayout); // 扫描区域
		scanView = (ScannerRelativeLayout) findViewById(R.id.scanner);// 扫描控件
		showTextView = (TextView) findViewById(R.id.textView1);
	//	Print = (EditText) findViewById(R.id.print);
		Wenzi = (Button) findViewById(R.id.weizi);
		MySelf = (Button) findViewById(R.id.myself);
		Code = (Button) findViewById(R.id.code);
		BtICcard = (Button) findViewById(R.id.id_title);
		BtMsr = (Button) findViewById(R.id.id_title2s);
		BtRfid = (Button) findViewById(R.id.id_title3s);
		BtPrinter = (Button) findViewById(R.id.id_title4s);
		Btpsam = (Button) findViewById(R.id.id_title5s);
		Btclean = (Button) findViewById(R.id.id_title8s);
		gprs = (Button) findViewById(R.id.id_title6s);
		saomiao = (Button) findViewById(R.id.id_title7s);
		BtICcard.setOnClickListener((OnClickListener) this);
		BtMsr.setOnClickListener((OnClickListener) this);
		BtRfid.setOnClickListener((OnClickListener) this);
		BtPrinter.setOnClickListener((OnClickListener) this);
		Btpsam.setOnClickListener((OnClickListener) this);
		Btclean.setOnClickListener((OnClickListener) this);
		gprs.setOnClickListener((OnClickListener) this);
		saomiao.setOnClickListener((OnClickListener) this);
		Wenzi.setOnClickListener(ThisPrint);
		MySelf.setOnClickListener(ThisPrint);
		Code.setOnClickListener(ThisPrint);

		handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				switch (msg.what) {

				case 1:
					showTextView.append(msg.obj.toString());
					showTextView.append("\r\n");

				case 2:
					switch (msg.arg1) {
					case 1:
						BtRfid.setEnabled(true);
						break;
					case 2:
						BtMsr.setEnabled(true);
						break;
					}
					break;

				}
			}
		};

	}

	private OnClickListener ThisPrint = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.weizi:
				if (printer.PrinterOpen() < 0) {
					return;
				}

				// printer.PrinterWrite(printer.printSelf());
				// printQRCodeItem(MainActivity.this);
				// String kotomi=Print.getText().toString();
				String kotomi = "xiekai misaka 好帅";
				// if(kotomi.equals("")){
				// Toast.makeText(MainActivity.this, "请输入内容",
				// Toast.LENGTH_SHORT).show();
				//
				//
				// }else{
				String s = sb(kotomi);
				final byte[] b = StringByte.HexString2Bytes(s);
				// 加粗
				byte y[] = { (byte) 0x1B, (byte) 0x45, (byte) 0x01 };
				// 不加
				byte z[] = { (byte) 0x1B, (byte) 0x45, (byte) 0x00 };
				byte x[] = { (byte) 0x1b, (byte) 0x0e };
				 printer.PrinterWrite(printer.getCmdEscAN(1));//右
				 printer.PrinterWrite(b);
				 printer.PrinterWrite(printer.getCmdLf());
				 printer.PrinterWrite(printer.getCmdEscAN(0));//zhong
				 printer.PrinterWrite(b);
				 printer.PrinterWrite(printer.getCmdLf());
				 printer.PrinterWrite(printer.getCmdEscAN(2));//left
				 printer.PrinterWrite(b);
				 printer.PrinterWrite(printer.getCmdLf());
				 printer.PrinterWrite(y);
				 printer.PrinterWrite(b);
				 printer.PrinterWrite(printer.getCmdLf());
				 printer.PrinterWrite(z);
				 printer.PrinterWrite(x);
				 printer.PrinterWrite(b);
				 printer.PrinterWrite(printer.getCmdLf());
				 printer.PrinterWrite(printer.getCmdLf());
				printrSelf p = new printrSelf();
				p.PrintOursSelf();
				printer.PrinterClose();
				Print.setText("");
				break;

			case R.id.myself:
				printer.PrinterOpen();
				printer.PrinterWrite(printer.getCmdEscSo());
				 printer.PrinterClose();
				break;
			case R.id.code:
				// if (printer.PrinterOpen() < 0) {
				// return;
				// }

			//	printer.PrinterWrite(printer.getCmdEscSo());
				PrintString p2 = new PrintString(getApplicationContext());
				p2.printours();
			

			}

		}
	};

	public byte[] Bitmap2Bytes(Bitmap bm) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
		return baos.toByteArray();
	}

	public static byte[] byteMerger(byte[] byte_1, byte[] byte_2) {
		byte[] byte_3 = new byte[byte_1.length + byte_2.length];
		System.arraycopy(byte_1, 0, byte_3, 0, byte_1.length);
		System.arraycopy(byte_2, 0, byte_3, byte_1.length, byte_2.length);
		return byte_3;
	}

	public static final int BIT_WIDTH = 384;

	private static final int WIDTH = 48;
	private static final int DOT_LINE_LIMIT = 200;
	private static final int DC2V_HEAD = 4;
	private static final int GSV_HEAD = 8;

	private static byte[] generateBitmapArrayGSV_MSB(Bitmap bm,
			int bitMarginLeft, int bitMarginTop) {
		byte[] result = null;
		int n = bm.getHeight() + bitMarginTop;
		int offset = GSV_HEAD;
		result = new byte[n * WIDTH + offset];
		for (int y = 0; y < bm.getHeight(); y++) {
			for (int x = 0; x < bm.getWidth(); x++) {
				if (x + bitMarginLeft < BIT_WIDTH) {
					int color = bm.getPixel(x, y);
					int alpha = Color.alpha(color);
					int red = Color.red(color);
					int green = Color.green(color);
					int blue = Color.blue(color);
					if (red < 128) {
						int bitX = bitMarginLeft + x;
						int byteX = bitX / 8;
						int byteY = y + bitMarginTop;
						result[offset + byteY * WIDTH + byteX] |= (0x80 >> (bitX - byteX * 8));
					}
				} else {
					// ignore the rest data of this line
					break;
				}
			}
		}
		return result;
	}

	/**
	 * 显示扫描区域
	 */
	public static void showSurfaceView() {
		showTextView.setVisibility(View.GONE);
		surfaceLayout.setVisibility(View.VISIBLE);
	}

	/**
	 * 显示扫描区域并且开始扫描
	 */
	public void showAndScan() {
		// 微信支付
		showSurfaceView();
		scanView.onResume();
		scanView.startScan();
		scanLisenter = new ScanSuccesListener();
		scanView.setScanSuccessListener(scanLisenter);
	}

	/**
	 * 监听扫描 成功后的结果处理
	 * 
	 * @author lenovo
	 * 
	 */
	private class ScanSuccesListener extends IScanEvent {
		@Override
		public void scanCompleted(ScannerResult scannerResult) {
			scanResultMy = scannerResult.getResult();
			System.out.println(scanResultMy + "misakmikoto");
			showTextView.setText("读取成功，二维码数据为：" + scanResultMy);
			showTextView.setVisibility(View.VISIBLE);
			surfaceLayout.setVisibility(View.GONE);
			scanView.stopScan();

		}

	}

	public static void printQRCodeItem(Activity host) {
		try {

			// Bitmap
			// bm=BitmapFactory.decodeFile("/data/data/com.wizarpos.printer/test2dbarcode.png");
			Bitmap bm = BitmapFactory.decodeResource(host.getResources(),
					R.drawable.test2dbarcode);
			// Bitmap
			// bm=BitmapFactory.decodeResource(getResources(),R.drawable.triangle_center);
			PrinterBitmapUtil.printBitmap(bm, 0, 0);
		} catch (Exception e) {
			// LogHelper.infoAppendMsgForFailed("无二维码文件");
			System.out.println("无二维码文件");
		}
	}

	private void saomiao() {
		showTextView.setVisibility(View.VISIBLE);
		surfaceLayout.setVisibility(View.GONE);
		scanView.stopScan();
	}

	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.id_title:
			miska();
			showTextView.setText("");
			ICcardtest();

			break;
		case R.id.id_title2s:
			miska();
			showTextView.setText("");
			msrtest();

			break;
		case R.id.id_title3s:
			miska();
			showTextView.setText("");
			rfidtest();

			break;
		case R.id.id_title4s:
			miska();
			showTextView.setText("");
			printertest();

			break;
		case R.id.id_title5s:
			miska();
			showTextView.setText("");
			psamcardtest();

			break;
		case R.id.id_title6s:
			miska();
			showTextView.setText("");
			showAndScan();

			break;
		case R.id.id_title7s:     
			miska();   
			showTextView.setText("");
			gprsAndNetwork();

			break;
		case R.id.id_title8s:
			miska();
			showTextView.setText("");
			saomiao();

			break;

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/**
	 * 开始测试
	 * 
	 * @param host
	 */
	public void scanQRCode(Activity host) {

		Intent intent = new Intent(host,
				com.wizarpos.barcode.scanner.ScannerActivity.class);
		intent.putExtra("decodeformat", "QR_CODE");
		host.startActivityForResult(intent, 107);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	void msrtest() {
		BtMsr.setEnabled(false);

		new Thread(new Runnable() {

			@Override
			public void run() {
				int ret;
				int status;
				int len;
				int times = 1000;
				boolean getData = false;

				byte[] track = new byte[128];
				String track1;
				String track2;
				String track3;
				while (--times > 0) {
					MSR.msr_open();
					ret = MSR.msr_poll(10);
					Log.i(getLocalClassName(), Integer.toString(ret));

					if (0 == ret) {
						status = MSR.msr_get_track_error(1);
						if (0x06 == status) {
							len = MSR.msr_get_track_data_length(1);
							Log.i(getLocalClassName(), Integer.toString(len));

							MSR.msr_get_track_data(1, track, len);
							track1 = "track1:" + new String(track, 0, len);
							Log.i(getLocalClassName(), track1);
							Message message = Message.obtain();
							message.obj = track1;
							message.arg1 = track1.length();
							message.what = 1;
							handler.sendMessage(message);

							getData = getData || true;
						}

						status = MSR.msr_get_track_error(2);
						if (0x06 == status) {
							len = MSR.msr_get_track_data_length(2);
							Log.i(getLocalClassName(), Integer.toString(len));

							MSR.msr_get_track_data(2, track, len);
							track2 = "track2:" + new String(track, 0, len);
							Log.i(getLocalClassName(), track2);
							Message message = Message.obtain();
							message.obj = track2;
							message.arg1 = track2.length();
							message.what = 1;
							handler.sendMessage(message);

							getData = getData || true;
						}

						status = MSR.msr_get_track_error(3);
						if (0x06 == status) {
							len = MSR.msr_get_track_data_length(3);
							Log.i(getLocalClassName(), Integer.toString(len));

							MSR.msr_get_track_data(3, track, len);
							track3 = "track3:" + new String(track, 0, len);
							Log.i(getLocalClassName(), track3);
							Message message = Message.obtain();
							message.obj = track3;
							message.arg1 = track3.length();
							message.what = 1;
							handler.sendMessage(message);
							getData = getData || true;
						}
					}

					MSR.msr_close();
					if (getData) {
						break;
					}
				}

				Message message = Message.obtain();

				message.what = 2;
				message.arg1 = 2;
				handler.sendMessage(message);
				getData = false;
			}

		}).start();

	}

	void rfidtest() {

		BtRfid.setEnabled(false);

		new Thread(new Runnable() {

			@Override
			public void run() {
				int times = 1000;

				byte[] cardType = new byte[4];

				if (0 == contactlessCard.RFIDModuleOpen()) {

					while (--times > 0) {

						String sPSNR = contactlessCard.MifareGetSNR(cardType);
						if (null != sPSNR) {
							String sendData = new String(sPSNR);

							sendData += "\r\n";
							sendData += String
									.format("%06x",
											(int) ((cardType[0] & 0xFF) << 16
													| (cardType[1] & 0xFF) << 8 | (cardType[2] & 0xFF)));

							Message message = Message.obtain();

							message.what = 1;
							message.obj = sendData;
							handler.sendMessage(message);

						} else {

							try {
								Thread.sleep(100);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							continue;
						}

						if ((cardType[0] & 0x20) != 0) {

							String sRATS = contactlessCard.TypeA_RATS();
							if (null != sRATS) {
								Message message = Message.obtain();
								message.what = 1;
								message.obj = sRATS;
								handler.sendMessage(message);

							} else {
								break;
							}

							String[] sapdu = contactlessCard
									.RFID_APDU(new String("0084000008"));
							if (null != sapdu) {
								String sendData = new String(sapdu[0]);
								sendData += "\r\n";
								sendData += sapdu[1];

								Message message = Message.obtain();
								message.what = 1;
								message.obj = sendData;
								handler.sendMessage(message);

							} else {
								break;
							}
							break;
						} else {
							byte[] key = { (byte) 0xff, (byte) 0xff,
									(byte) 0xff, (byte) 0xff, (byte) 0xff,
									(byte) 0xff };

							if (contactlessCard.RFIDAuthenCard((byte) 0,
									(byte) 0x0a, key) != 0) {
								break;
							}

							byte[] buf = new byte[16];
							if (contactlessCard.RFIDReadCard((byte) 0, buf) != 0) {
								break;
							}

							String str = halTrans.bytesToHexString(buf);
							str += "\r\n";

							if (contactlessCard.RFIDReadCard((byte) 1, buf) != 0) {
								break;
							}

							String str1 = halTrans.bytesToHexString(buf);

							str += str1;
							str += "\r\n";

							byte[] wrbuf = { '1', '1', '1', '1', '1', '1', '1',
									'1', '1', '1', '1', '1', '1', '1', '1', '1' };

							if (contactlessCard.RFIDWriteCard((byte) 2, wrbuf) != 0) {
								break;
							}

							if (contactlessCard.RFIDReadCard((byte) 2, buf) != 0) {
								break;
							}

							String str2 = halTrans.bytesToHexString(buf);
							str += str2;
							str += "\r\n";

							if (contactlessCard.RFIDInitValue((byte) 2, 3) != 0) {
								break;
							}

							if (contactlessCard.RFIDReadCard((byte) 2, buf) != 0) {
								break;
							}

							String str3 = halTrans.bytesToHexString(buf);
							str += str3;
							str += "\r\n";

							if (contactlessCard.RFIDInctValue((byte) 2, 1) != 0) {
								break;
							}

							if (contactlessCard.RFIDReadCard((byte) 2, buf) != 0) {
								break;
							}

							String str4 = halTrans.bytesToHexString(buf);
							str += str4;
							str += "\r\n";

							if (contactlessCard.RFIDDectValue((byte) 2, 1) != 0) {
								break;
							}

							if (contactlessCard.RFIDReadCard((byte) 2, buf) != 0) {
								break;
							}

							String str5 = halTrans.bytesToHexString(buf);
							str += str5;
							str += "\r\n";

							if (contactlessCard.RFIDRestor((byte) 2, (byte) 1) != 0) {
								break;
							}

							if (contactlessCard.RFIDReadCard((byte) 1, buf) != 0) {
								break;
							}

							String str6 = halTrans.bytesToHexString(buf);
							str += str6;
							str += "\r\n";
							if (contactlessCard.RFIDDectValue((byte) 1, 1) != 0) {
								break;
							}

							if (contactlessCard.RFIDReadCard((byte) 1, buf) != 0) {
								break;
							}

							String str7 = halTrans.bytesToHexString(buf);
							str += str7;

							Message message = Message.obtain();
							message.what = 1;
							message.obj = str;
							handler.sendMessage(message);
							break;
						}

					}

					contactlessCard.RFIDMoudleClose();

					Message message = new Message();

					message.what = 2;
					message.arg1 = 1;
					handler.sendMessage(message);

				}

			}
		}).start();

	}

	void printertest() {
		Print.setVisibility(View.VISIBLE);
		MySelf.setVisibility(View.VISIBLE);
		Wenzi.setVisibility(View.VISIBLE);
		Code.setVisibility(View.VISIBLE);
		showTextView.setVisibility(View.GONE);

	}

	private void miska() {
		Print.setVisibility(View.GONE);
		MySelf.setVisibility(View.GONE);
		Wenzi.setVisibility(View.GONE);
		showTextView.setVisibility(View.VISIBLE);

		surfaceLayout.setVisibility(View.GONE);
		scanView.stopScan();
	}

	private static String sb(String content) {
		String str = content;

		String hexString = "0123456789ABCDEF";
		byte[] bytes;
		try {
			bytes = str.getBytes("GBK");// 如果此处不加编码转化，得到的结果就不是理想的结果，中文转码
			StringBuilder sb = new StringBuilder(bytes.length * 2);

			for (int i = 0; i < bytes.length; i++) {
				sb.append(hexString.charAt((bytes[i] & 0xf0) >> 4));
				sb.append(hexString.charAt((bytes[i] & 0x0f) >> 0));
				// sb.append("");
			}
			str = sb.toString();

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;

	}

	void psamcardtest() {

		psam.OpenPsamMoudle();
		String str1 = psam.psamCardReset(9600, 0);
		if (null != str1) {
			showTextView.append("slot0:");
			showTextView.append(str1);
			showTextView.append("\r\n");
			String[] sapdu = psam.psamCardSendAPDUT0(0,
					new String("0084000008"));
			showTextView.append(sapdu[0]);
			showTextView.append("\r\n");
			showTextView.append(sapdu[1]);
			showTextView.append("\r\n");
		}
		String str2 = psam.psamCardReset(9600, 1);
		if (null != str2) {
			showTextView.append("slot1:");
			showTextView.append(str2);
			showTextView.append("\r\n");
			String[] sapdu = psam.psamCardSendAPDUT0(1,
					new String("0084000008"));
			showTextView.append(sapdu[0]);
			showTextView.append("\r\n");
			showTextView.append(sapdu[1]);
			showTextView.append("\r\n");
		}
		String str3 = psam.psamCardReset(9600, 2);
		if (null != str3) {
			showTextView.append("slot2:");
			showTextView.append(str3);
			showTextView.append("\r\n");
			String[] sapdu = psam.psamCardSendAPDUT0(2,
					new String("0084000008"));
			showTextView.append(sapdu[0]);
			showTextView.append("\r\n");
			showTextView.append(sapdu[1]);
			showTextView.append("\r\n");
		}
		String str4 = psam.psamCardReset(9600, 3);
		if (null != str4) {
			showTextView.append("slot3:");
			showTextView.append(str4);
			showTextView.append("\r\n");
			String[] sapdu = psam.psamCardSendAPDUT0(3,
					new String("0084000008"));
			showTextView.append(sapdu[0]);
			showTextView.append("\r\n");
			showTextView.append(sapdu[1]);
			showTextView.append("\r\n");
		}

		String str5 = psam.psamCardReset(9600, 4);
		if (null != str5) {
			showTextView.append("slot4:");
			showTextView.append(str5);
			showTextView.append("\r\n");
			String[] sapdu = psam.psamCardSendAPDUT0(4,
					new String("0084000008"));
			showTextView.append(sapdu[0]);
			showTextView.append("\r\n");
			showTextView.append(sapdu[1]);
			showTextView.append("\r\n");

		}
		psam.ClosePsamModule();
	}

	void ICcardtest() {
		String str;
		ICcard.ICcardOpen();
		if ((str = ICcard.CpuCardPowerOn((byte) 0)) != null) {
			showTextView.append("slot0:\r\n");
			showTextView.append(str + "\r\n");
			String[] recv;
			recv = ICcard.CpuCardSendAPDU((byte) 0, new String("0084000008"));
			if (recv == null) {
				showTextView.append("数据为空");

			} else {

				showTextView.append(recv[0]);
				showTextView.append("\r\n");
				showTextView.append(recv[1]);
				showTextView.append("\r\n");
			}
		}

		ICcard.CpuCardPowerOff((byte) 0);

		ICcard.ICcardClose();

	}

	/**
	 * 联网检测
	 */
	protected void gprsAndNetwork() {

		StringBuilder say = new StringBuilder();

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED
				|| cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
						.getState() == NetworkInfo.State.CONNECTING) {
			say.append("GPRS state: connected\r\n");
		} else {
			say.append("GPRS state: not connect\r\n");
		}

		if (cm.getNetworkInfo(ConnectivityManager.TYPE_ETHERNET).getState() == NetworkInfo.State.CONNECTED
				|| cm.getNetworkInfo(ConnectivityManager.TYPE_ETHERNET)
						.getState() == NetworkInfo.State.CONNECTING) {
			say.append("Network state: connected\r\n");
		} else {
			say.append("Network state: not connect\r\n");
		}

		if (cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED
				|| cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTING) {
			say.append("WIFI state: connected\r\n");
		} else {
			say.append("WIFI state: not connect\r\n");
		}

		showTextView.setText(say.toString());

	}

}
