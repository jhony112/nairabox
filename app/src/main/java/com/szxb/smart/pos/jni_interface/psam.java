package com.szxb.smart.pos.jni_interface;

import android.util.Log;

public class psam extends halTrans {

	static {
		try {
			System.loadLibrary("PsamCard");
		} catch (Throwable e) {
			Log.e("jni", "i can't find PsamCard so!");
			e.printStackTrace();
		}
	}
	
		
	public static native int OpenPsamMoudle();
	public static native int ClosePsamModule();
	public static native String psamCardReset(int baud,int slot);
	public static native String[] psamCardSendAPDUT0(int slot ,String sendApdu);
}
