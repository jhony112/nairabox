package com.swipemax.nairabox.Utils;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.swipemax.nairabox.HomeActivity;
import com.swipemax.nairabox.PrintString;
import com.swipemax.nairabox.R;
import com.szxb.smart.pos.jni_interface.printer;
import com.wizarpos.barcode.scanner.IScanEvent;
import com.wizarpos.barcode.scanner.ScannerRelativeLayout;
import com.wizarpos.barcode.scanner.ScannerResult;

import java.util.ArrayList;

/**
 * Created by samwul on 29/08/2016.
 */
public class ListAdapter extends BaseAdapter {

    ArrayList<Ticket> ticketList;
    HomeActivity activity;
    public static FrameLayout surfaceLayout;
    public static ScannerRelativeLayout scanView;
    public IScanEvent scanLisenter;
    private String scanResultMy;
    private String currentTicketID;
    private Ticket currentTicket;

    public ListAdapter(HomeActivity activity, ArrayList<Ticket> ticketList){
        this.activity = activity;
        this.ticketList = ticketList;
    }

    public void setList(ArrayList<Ticket> ticketList){
        this.ticketList = ticketList;
    }

    @Override
    public int getCount(){
        return ticketList.size();
    }

    @Override
    public Object getItem(int location) {
        return ticketList.get(location);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public static void showSurfaceView() {
        //showTextView.setVisibility(View.GONE);
        surfaceLayout.setVisibility(View.VISIBLE);
    }

    public void showAndScan() {
        // 微信支付
        showSurfaceView();
        scanView.onResume();
        scanView.startScan();
        scanLisenter = new ScanSuccesListener();
        scanView.setScanSuccessListener(scanLisenter);
    }
    private class ScanSuccesListener extends IScanEvent {
        @Override
        public void scanCompleted(ScannerResult scannerResult) {
            scanResultMy = scannerResult.getResult();

            PrintString p2 = new PrintString(activity.getApplicationContext());
            if(!p2.hasPaper()){
                Toast.makeText(activity.getApplicationContext(), "Error. The Printer is out of paper", Toast.LENGTH_LONG).show();
            }
            else {
                if (scanResultMy.equalsIgnoreCase(currentTicketID)) {
                    p2.printTicket(currentTicket, activity);
                } else {
                    Toast.makeText(activity.getApplicationContext(), "Sorry, You scanned the wrong ticket", Toast.LENGTH_LONG).show();
                }
            }
        }

    }

    @Override
    public View getView(int position, View view, ViewGroup vgroup){
        Ticket t = ticketList.get(position);
        final int x = position;
        LayoutInflater inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v =  inflater.inflate(R.layout.ticket_list_row, null);
            TextView tv = (TextView) v.findViewById(R.id.tickettitle);
            tv.setText(t.getMovie());
            TextView id = (TextView) v.findViewById(R.id.ticketid);
            id.setText(position);

        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView id = (TextView) v.findViewById(R.id.ticketid);
                int tid = Integer.getInteger(id.getText().toString());
                Ticket ticket = ticketList.get(tid);
                currentTicketID = ticket.getId();
                showAndScan();
            }
        });
        return v;
    }
}
