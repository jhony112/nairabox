package com.swipemax.nairabox.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.swipemax.nairabox.R;
import com.swipemax.nairabox.Utils.Ticket;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Fragment_Details.OnFragmentDetailListener} interface
 * to handle interaction events.
 * Use the {@link Fragment_Details#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Fragment_Details extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String MOVIE_TITLE = "title";
    private static final String MOVIE_PRICE = "price";
    private static final String MOVIE_CLASS = "class";
    private static final String MOVIE_TIME = "time";
    private static final String MOVIE_CINEMA = "cinema";
    private static final String MOVIE_STATUS = "status";
    private static final String MOVIE_ID ="id" ;
    private static final String MOVIE_TICKET ="movieTicket" ;

    // TODO: Rename and change types of parameters

    private String m_title,m_price,m_class,m_time,m_cinema,m_id;
    private Ticket ticket;
    int m_status=1;
View _view;
    private OnFragmentDetailListener mListener;

    public Fragment_Details() {
        // Required empty public constructor
    }

    /**
     Single movie fragment
     */
    // TODO: Rename and change types and number of parameters
    public static Fragment_Details newInstance(int Status,String m_title,String m_price,String m_class,String m_time,String m_cinema,String m_id, Ticket ticket) {
        Fragment_Details fragment = new Fragment_Details();
        Bundle args = new Bundle();
        args.putString(MOVIE_TITLE, m_title);
        args.putString(MOVIE_PRICE, m_price);
        args.putString(MOVIE_CINEMA, m_cinema);
        args.putString(MOVIE_CLASS, m_class);
        args.putString(MOVIE_TIME, m_time);
        args.putInt(MOVIE_STATUS, Status);
        args.putString(MOVIE_ID, m_id);
        args.putParcelable(MOVIE_TICKET, ticket);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            m_title = getArguments().getString(MOVIE_TITLE);
            m_price = getArguments().getString(MOVIE_PRICE);
            m_class = getArguments().getString(MOVIE_CLASS);
            m_cinema = getArguments().getString(MOVIE_CINEMA);
            m_time = getArguments().getString(MOVIE_TIME);
            m_id = getArguments().getString(MOVIE_ID);
            m_status = getArguments().getInt(MOVIE_STATUS);
            ticket = getArguments().getParcelable(MOVIE_TICKET);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        _view= inflater.inflate(R.layout.fragment__details, container, false);
            Button accept = (Button) _view.findViewById(R.id.btn_accept);
            accept.setOnClickListener(this);
        return _view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentDetailListener) {
            mListener = (OnFragmentDetailListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view){
        mListener.onTicketPrint(ticket);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentDetailListener {
        // TODO: Update argument type and name
        void onTicketPrint(Ticket ticket);
    }
}
