package com.swipemax.nairabox.Utils;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by samwul on 25/08/2016.
 */
public class TicketClass implements Parcelable {
    private String tClass;
    private int quantity;
    private int price;

    public String gettClass() {
        return tClass;
    }

    public void settClass(String tClass) {
        this.tClass = tClass;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeString(this.gettClass());
        out.writeInt(this.getQuantity());
        out.writeInt(this.getPrice());
    }

    public static final Parcelable.Creator<TicketClass> CREATOR
            = new Parcelable.Creator<TicketClass>() {
        public TicketClass createFromParcel(Parcel in) {
            return new TicketClass(in);
        }

        public TicketClass[] newArray(int size) {
            return new TicketClass[size];
        }
    };

    private TicketClass(Parcel in) {
        this.settClass(in.readString());
        this.setQuantity(in.readInt());
        this.setPrice(in.readInt());
    }

    public TicketClass(){

    }

    @Override
    public String toString() {
        return this.gettClass();
    }
}
