package com.swipemax.nairabox;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.swipemax.nairabox.Utils.ListAdapter;
import com.swipemax.nairabox.Utils.Ticket;
import com.swipemax.nairabox.Utils.TicketClass;
import com.szxb.smart.pos.jni_interface.printer;
import com.wizarpos.barcode.scanner.IScanEvent;
import com.wizarpos.barcode.scanner.ScannerRelativeLayout;
import com.wizarpos.barcode.scanner.ScannerResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class HomeActivity extends Activity {
	Button btn_test,btn_scan;
	public static FrameLayout surfaceLayout;
	public static ScannerRelativeLayout scanView;
	public IScanEvent scanLisenter;
	private String scanResultMy;
	private static TextView showTextView;
	private ListAdapter adapter;
	ArrayList<Ticket> ticketList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);

		surfaceLayout = (FrameLayout) findViewById(R.id.surfaceViewLayout); // 扫描区域
		scanView = (ScannerRelativeLayout) findViewById(R.id.scanner);// 扫描控件
		showTextView = (TextView) findViewById(R.id.textView);
		btn_test = (Button)findViewById(R.id.Test_btn);
		btn_scan = (Button)findViewById(R.id.btn_scanner);
		btn_scan.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				showAndScan();
			}
		});
		btn_test.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				PrintString p2 = new PrintString(getApplicationContext());
				p2.printours();
			}
		});

		Button listButton = (Button) findViewById(R.id.listtickets);
		listButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				adapter.setList(ticketList);
				adapter.notifyDataSetChanged();
			}
		});

		ticketList = new ArrayList();
		adapter = new ListAdapter(this, ticketList);
		ListView lv = (ListView) findViewById(R.id.listview);
		lv.setAdapter(adapter);
	}

	public void removeTicket(Ticket ticket){
		ticketList.remove(ticket);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}
	public static void showSurfaceView() {
		//showTextView.setVisibility(View.GONE);
		surfaceLayout.setVisibility(View.VISIBLE);
	}
	public void showAndScan() {
		// 微信支付
		showSurfaceView();
		scanView.onResume();
		scanView.startScan();
		scanLisenter = new ScanSuccesListener();
		scanView.setScanSuccessListener(scanLisenter);
	}
	private class ScanSuccesListener extends IScanEvent {
		@Override
		public void scanCompleted(ScannerResult scannerResult) {
			scanResultMy = scannerResult.getResult();
			//	System.out.println(scanResultMy + "misakmikoto");
			showTextView.setText("Result is ：" + scanResultMy);
			showTextView.setVisibility(View.VISIBLE);
			surfaceLayout.setVisibility(View.GONE);
			scanView.stopScan();

			PrintString p2 = new PrintString(getApplicationContext());
			if(!p2.hasPaper()){
				Toast.makeText(getApplicationContext(), "Error. The Printer is out of paper", Toast.LENGTH_LONG).show();
			}else {

				apiTask task = new apiTask();
				task.execute(scanResultMy);
			}

		}

	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			surfaceLayout.setVisibility(View.GONE);
			scanView.stopScan();
		}
		return super.onOptionsItemSelected(item);
	}

	class apiTask extends AsyncTask<String, String, JSONObject>{

		ProgressDialog progressDialog;


		public apiTask(){

		}

		@Override
		public void onPreExecute(){
			// show a loading message
			progressDialog = ProgressDialog.show(HomeActivity.this, "nairabox", "processing...");
			//progressDialog.setMessage("processing...");
			//progressDialog.show();
		}

		@Override
		public JSONObject doInBackground(String... params){
			// do apiCall to verify code
			String code = params[0];
			System.out.println("code is - " + code);

			String url = "https://revision.nairabox.com/v1/tickets/scanner.php?tid=" + code;
			try {

				URL obj = new URL(url);
				HttpURLConnection con = (HttpURLConnection) obj.openConnection();

				//add reuqest header
				con.setRequestMethod("GET");
				con.setRequestProperty("User-Agent", "Mozilla/5.0");
				con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
				//con.setConnectTimeout(7000);

				int responseCode = con.getResponseCode();
				System.out.println(responseCode);

				if(responseCode != 200){ return null; }

				BufferedReader in = new BufferedReader( new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				String finalResponse = response.toString();

				System.out.println( "final response - " + finalResponse);

				JSONObject ret = new JSONObject(finalResponse);

				return ret;
			}
			catch(Exception e){
				e.printStackTrace();
				return null;
			}
		}

		@Override
		public void onPostExecute(JSONObject result){
			// redact loading message
			// print ticket or show error message
			progressDialog.dismiss();
			progressDialog = null;

			if(result != null){
				try{
					int status = result.getInt("status");
					if(status == 200){
						JSONObject ticket = result.getJSONObject("ticket");
						Ticket t = new Ticket();
						t.setCinema(result.getString("cinema"));
						t.setDate(ticket.getString("date"));

						TicketClass adult = new TicketClass();
						adult.settClass("Adult");
						adult.setQuantity(ticket.getInt("adults"));
						adult.setPrice(result.getInt("adlt_price"));

						TicketClass student = new TicketClass();
						student.settClass("Student");
						student.setQuantity(ticket.getInt("students"));
						student.setPrice(result.getInt("sdnt_price"));

						TicketClass children = new TicketClass();
						children.settClass("Children");
						children.setQuantity(ticket.getInt("children"));
						children.setPrice(result.getInt("chld_price"));

						ArrayList<TicketClass> tc = new ArrayList();
						tc.add(adult); tc.add(student); tc.add(children);

						t.setClasses(tc);

						t.setMovie(result.getString("movie"));
						t.setTime(ticket.getString("date"));
						t.setTotalPaid(ticket.getInt("amount"));

						ticketList.add(t);


						PrintString p2 = new PrintString(getApplicationContext());
						p2.printTicket(t, ticketList);

					}else{
						Toast.makeText(getApplicationContext(), result.getString("message"), Toast.LENGTH_LONG).show();
					}
				}
				catch(JSONException e){
					e.printStackTrace();
					Toast.makeText(getApplicationContext(), "Error: Cannot parse JSON", Toast.LENGTH_LONG).show();
				}
				catch(Exception e){
					e.printStackTrace();
					Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
				}
			}else{
				Toast.makeText(getApplicationContext(), "It looks like the printer is out of paper", Toast.LENGTH_LONG).show();
			}
		}
	}
}
