package com.swipemax.nairabox.intro;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.swipemax.nairabox.R;
import com.swipemax.nairabox.fragment.FragmentLogin;

import java.util.HashMap;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class SplashActivity2 extends FragmentActivity implements FragmentLogin.FragmemtLoginListener {
    private static final int NUM_PAGES = 5;

    private ViewPager mPager;

    private PagerAdapter mPagerAdapter;
    private SliderLayout mDemoSlider;
    private PagerIndicator mCustomIndicator;
    private Button btn_login;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    void setWindow() {
        final Window win = getWindow();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        win.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setWindow();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);

        //  Toast.makeText(SplashActivity2.this,"Value :"+ screenHeight +"width "+ screenWidth+"height ", Toast.LENGTH_SHORT).show();
        mDemoSlider = (SliderLayout) findViewById(R.id.slider);
        btn_login = (Button) findViewById(R.id.btn_login);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentLogin login=new FragmentLogin();
                updateFragment(login,"login");
            }
        });
        mCustomIndicator = (PagerIndicator) findViewById(R.id.custom_indicator);
        HashMap<String, Integer> file_maps = new HashMap<String, Integer>();
        file_maps.put("one", R.drawable.slide2);
        file_maps.put("two", R.drawable.slide1);
        file_maps.put("three", R.drawable.slide3);


        for (String name : file_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(this);
            // initialize a SliderLayout
            textSliderView
                    .description("")
                    .image(file_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {

                        }
                    });

            //add your extra information
//            textSliderView.bundle(new Bundle());
//            textSliderView.getBundle()
//                    .putString("extra",name);

            mDemoSlider.addSlider(textSliderView);
        }
        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
        mDemoSlider.setCustomIndicator(mCustomIndicator);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(4000);
        mDemoSlider.addOnPageChangeListener(new ViewPagerEx.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    @Override
    protected void onStop() {
        // To prevent a memory leak on rotation, make sure to call stopAutoCycle() on the slider before activity or fragment is destroyed
        mDemoSlider.stopAutoCycle();
        super.onStop();
    }

    private void updateFragment(Fragment frg, String tag) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(android.R.id.content, frg, tag);
        fragmentTransaction.commit();
    }

    @Override
    public void onLogin(String id, String password) {
        Toast.makeText(SplashActivity2.this, "user :"+id +"password "+password, Toast.LENGTH_SHORT).show();
    }
}
