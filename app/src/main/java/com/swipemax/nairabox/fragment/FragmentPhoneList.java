package com.swipemax.nairabox.fragment;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.swipemax.nairabox.AppController;
import com.swipemax.nairabox.R;
import com.swipemax.nairabox.Utils.Ticket;

import java.util.ArrayList;

/**
 * Created by samwul on 17/09/2016.
 */
public class FragmentPhoneList extends DialogFragment implements View.OnClickListener {
    public FragmentPhoneList(){

    }

    @Override
    public void onStart() {
        super.onStart();
       /* d = getDialog();
        if (d != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            d.getWindow().setLayout(width, height);
            d.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        }*/

    }

    public static FragmentPhoneList newInstance() {
        FragmentPhoneList fragment = new FragmentPhoneList();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.MY_DIALOG);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_phone_list, container, false);
        ListView listView = (ListView) view.findViewById(R.id.phone_list_view);
        listView.setOnClickListener(this);

        ArrayAdapter<Ticket> adapter = new ArrayAdapter<Ticket>(getContext(), R.layout.fragment_phone_list_row, R.id.text_view_for_phone_ticket,
                AppController.getInstance().getMyTicketsByPhone());

        return view;
    }

    @Override
    public void onClick(View view) {
        ListView listView = (ListView) view.findViewById(R.id.phone_list_view);
        int pos = listView.getPositionForView(view);
        ArrayList<Ticket> tickets = AppController.getInstance().getMyTicketsByPhone();

        Ticket t = tickets.get(pos);
        Fragment_Details fd = Fragment_Details.newInstance(200, t.getMovie(),Integer.toString(t.getTotalPaid()), t.getClasses().toString(), t.getTime(), t.getCinema(), t.getId(), t);
        FragmentTransaction ft = fd.getFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_up, R.anim.slide_down);
        ft.show(fd);
        ft.commit();
    }
}
