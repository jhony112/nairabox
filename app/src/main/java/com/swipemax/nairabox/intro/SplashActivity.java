package com.swipemax.nairabox.intro;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;


import com.swipemax.nairabox.Utils.Constant;
import com.swipemax.nairabox.activity.BaseActivity;

/**
 * Created by Jhony112 on 2016/09/07.
 */
public class SplashActivity extends Activity {
    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (GetBoolPref(Constant.FIRST_RUN)) {
            Intent intent = new Intent(this, LandingPage.class);
            startActivity(intent);
            finish();
        } else {
            Intent home = new Intent(SplashActivity.this, BaseActivity.class);
            home.putExtra(Constant.MERCHANT_NAME, GetPref(Constant.MERCHANT_NAME));
            home.putExtra(Constant.TERMINAL_ID, GetPref(Constant.TERMINAL_ID));
            home.putExtra(Constant.DISPLAY_PHOTO, GetPref(Constant.DISPLAY_PHOTO));
            startActivity(home);
            finish();
        }
    }

    boolean GetBoolPref(String name) {
        prefs = getSharedPreferences(Constant.TAG_APP_NAME, MODE_PRIVATE);
        return prefs.getBoolean(name, true);
    }

    String GetPref(String name) {
        prefs = getSharedPreferences(Constant.TAG_APP_NAME, MODE_PRIVATE);
        return prefs.getString(name, "");
    }
}
