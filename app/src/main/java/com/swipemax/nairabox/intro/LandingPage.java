package com.swipemax.nairabox.intro;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.swipemax.nairabox.AppController;
import com.swipemax.nairabox.PrintString;
import com.swipemax.nairabox.R;
import com.swipemax.nairabox.Utils.Constant;
import com.swipemax.nairabox.Utils.CustomRequest;
import com.swipemax.nairabox.Utils.Ticket;
import com.swipemax.nairabox.Utils.TicketClass;
import com.swipemax.nairabox.activity.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LandingPage extends Activity {
    private Button btn_login;
    private EditText merchant_ID;
    private EditText merchant_Pin;
    private RelativeLayout overlay;
    private LinearLayout main_layout;
    private SharedPreferences prefs;


    void setWindow() {
        final Window win = getWindow();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        win.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setWindow();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_page);
        btn_login = (Button) findViewById(R.id.btn_login);
        overlay = (RelativeLayout) findViewById(R.id.overlay);
        main_layout = (LinearLayout) findViewById(R.id.main_content);
        merchant_ID = (EditText) findViewById(R.id.input_merchantID);
        merchant_Pin = (EditText) findViewById(R.id.input_merchantPin);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String m_id = merchant_ID.getText().toString().trim();
                String m_pin = merchant_Pin.getText().toString().trim();
                if (m_id.length() > 0 && m_pin.length() > 0) {

                    // Login(m_id, m_pin);
                    apiTask task = new apiTask();
                    task.execute(m_id, m_pin);

                } else {
                    Toast.makeText(LandingPage.this, "Please fill all fields", Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    void Login(String username, String password) {

        // String url = Constant.LOGIN_URL;
        Map<String, String> params = new HashMap<String, String>();
        params.put("uid", username);
        params.put("pin", password);
        params.put("case", "merchant");
        Uri builtUri = Uri.parse(Constant.LOGIN_URL)
                .buildUpon()
                .appendQueryParameter("uid", username)
                .appendQueryParameter("pin", password)
                .appendQueryParameter("case", "login")
                .build();

        CustomRequest jsObjRequest = new CustomRequest(Request.Method.POST, builtUri.toString(), null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {


                // VolleyLog.d("Response is: ", response.toString());

                if (response != null) {
                    try {
                        int status = response.getInt("status");
                        // VolleyLog.d("Response is: ", status);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                overlay.setVisibility(View.INVISIBLE);
                main_layout.setEnabled(true);


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError response) {
                Log.d("Response: ", response.toString());
            }
        });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(5 * DefaultRetryPolicy.DEFAULT_TIMEOUT_MS, 0, 0));
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(0, 0, 0));
        AppController.getInstance().addToRequestQueue(jsObjRequest);
    }

    class apiTask extends AsyncTask<String, String, JSONObject> {

        ProgressDialog progressDialog;


        public apiTask() {

        }

        @Override
        public void onPreExecute() {

            overlay.setVisibility(View.VISIBLE);
            enableViews(main_layout, false);
        }

        @Override
        public JSONObject doInBackground(String... params) {
            // do apiCall to verify code
            Uri builtUri = Uri.parse(Constant.LOGIN_URL)
                    .buildUpon()
                    .appendQueryParameter("uid", params[0])
                    .appendQueryParameter("pin", params[1])
                    .appendQueryParameter("case", "login")
                    .build();
            try {

                URL obj = new URL(builtUri.toString());
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();

                //add reuqest header
                con.setRequestMethod("POST");
                con.setRequestProperty("User-Agent", "Mozilla/5.0");
                con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
                //con.setConnectTimeout(7000);

                int responseCode = con.getResponseCode();
                System.out.println(responseCode);

                if (responseCode != 200) {
                    return null;
                }

                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                String finalResponse = response.toString();

                System.out.println("final response - " + finalResponse);

                JSONObject ret = new JSONObject(finalResponse);

                return ret;
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(JSONObject result) {
            if (result != null) {
                try {
                    int status = result.getInt("status");
                    if (status == 200) {
                        String photo = result.getString("photo");
                        String term_id = result.getString("terminalID");
                        String name = result.getString("name");
                        AddPref(Constant.DISPLAY_PHOTO, photo);
                        AddPref(Constant.TERMINAL_ID, term_id);
                        AddPref(Constant.MERCHANT_NAME, name);
                        AddBooleanPref(Constant.FIRST_RUN, false);

                        enableViews(main_layout, true);
                        startMain(photo, name, term_id);
                        //   Toast.makeText(getApplicationContext(), name+photo, Toast.LENGTH_LONG).show();

                    } else {
                        Toast.makeText(getApplicationContext(), result.getString("message"), Toast.LENGTH_LONG).show();
                        enableViews(main_layout, false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Error: Cannot parse JSON", Toast.LENGTH_LONG).show();
                    enableViews(main_layout, false);
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    enableViews(main_layout, false);
                }
            } else {
                Toast.makeText(getApplicationContext(), "It looks like the network is shaky", Toast.LENGTH_LONG).show();
                overlay.setVisibility(View.INVISIBLE);
                main_layout.setEnabled(true);
            }
            overlay.setVisibility(View.INVISIBLE);
            main_layout.setEnabled(true);
        }


    }

    void AddPref(String name, String value) {
        prefs = getSharedPreferences(Constant.TAG_APP_NAME, MODE_PRIVATE);
        prefs.edit().putString(name, value).apply();
    }

    String GetPref(String name) {
        prefs = getSharedPreferences(Constant.TAG_APP_NAME, MODE_PRIVATE);
        return prefs.getString(name, "");
    }

    void AddBooleanPref(String name, boolean val) {
        prefs = getSharedPreferences(Constant.TAG_APP_NAME, MODE_PRIVATE);
        prefs.edit().putBoolean(name, val).apply();
    }

    boolean GetBoolPref(String name) {
        prefs = getSharedPreferences(Constant.TAG_APP_NAME, MODE_PRIVATE);
        return prefs.getBoolean(name, true);
    }

    private void enableViews(View v, boolean enabled) {
        if (v instanceof ViewGroup) {
            ViewGroup vg = (ViewGroup) v;
            for (int i = 0; i < vg.getChildCount(); i++) {
                enableViews(vg.getChildAt(i), enabled);
            }
        }
        v.setEnabled(enabled);
    }

    void startMain(String photo, String name, String id) {
        Intent main_act = new Intent(LandingPage.this, BaseActivity.class);
        main_act.putExtra(Constant.MERCHANT_NAME, name);
        main_act.putExtra(Constant.TERMINAL_ID, id);
        main_act.putExtra(Constant.DISPLAY_PHOTO, photo);
        startActivity(main_act);
    }
}

