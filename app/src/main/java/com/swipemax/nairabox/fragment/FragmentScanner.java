package com.swipemax.nairabox.fragment;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;

import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.VideoView;

import com.swipemax.nairabox.R;
import com.wizarpos.barcode.scanner.IScanEvent;
import com.wizarpos.barcode.scanner.ScannerRelativeLayout;
import com.wizarpos.barcode.scanner.ScannerResult;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * <p/>
 * Use the {@link FragmentScanner#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentScanner extends DialogFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    public static ScannerRelativeLayout scanView;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View _view;
    private FragmentScannerListener mListener;
    private Dialog d;
    private MediaController media_control;
    private VideoView video_view;
    private ScanSuccesListener scanLisenter;
    private ImageView btn_back;

    public FragmentScanner() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        d = getDialog();
        if (d != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            d.getWindow().setLayout(width, height);
            d.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        }

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentScanner.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentScanner newInstance() {
        FragmentScanner fragment = new FragmentScanner();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.MY_DIALOG);


    }

    void setBackHandle(View view) {
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                Log.i("nairabox", "keyCode: " + keyCode);
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    Log.i("narabox", "onKey Back listener is working!!!");
                    if (scanView != null) {
                        scanView.stopScan();
                    }
                    return true;
                } else {
                    return false;
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        _view = inflater.inflate(R.layout.fragment_scanner, container, false);
        //setBackHandle(_view);
//         video_view = (VideoView) _view.findViewById(R.id.video_player_view);
//
//        Uri uri = Uri.parse("android.resource://" + getActivity().getPackageName() + "/" + R.raw.scan);
//
//        media_control = new MediaController(getActivity());
//        video_view.setMediaController(media_control);
//
//        video_view.setVideoURI(uri);
//        video_view.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//            @Override
//            public void onPrepared(MediaPlayer mp) {
//                mp.setLooping(true);
//            }
//        });
//        video_view.start();
        scanView = (ScannerRelativeLayout) _view.findViewById(R.id.my_scanner);
        btn_back = (ImageView) _view.findViewById(R.id.img_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
        runScan();
        return _view;
    }

    void runScan() {
        scanView.onResume();
        scanView.startScan();
        scanLisenter = new ScanSuccesListener();
        scanView.setScanSuccessListener(scanLisenter);

    }

    private class ScanSuccesListener extends IScanEvent {
        @Override
        public void scanCompleted(ScannerResult scannerResult) {

            mListener.OnScanComplete(scannerResult.getResult());

            scanView.stopScan();

        }

    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (scanView != null) {
            scanView.stopScan();
        }
        super.onDismiss(dialog);
    }
// TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentScannerListener) {
            mListener = (FragmentScannerListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnPhoneFragment");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface FragmentScannerListener {
        // TODO: Update argument type and name
        void OnScanComplete(String Result);

        void OnScanCancelled();

    }

}
