package com.szxb.smart.pos.jni_interface;

import android.util.Log;

public class contactlessCard extends halTrans {

	static {
		try {
			System.loadLibrary("ContactlessCard");
		} catch (Throwable e) {
			Log.e("jni", "i can't find ContactlessCard so!");
			e.printStackTrace();
		}
	}


	public native static int RFIDMoudleClose();

	public native static String MifareGetSNR(byte[] cardType);

	public native static String TypeA_RATS();//cpu 

	public native static String[] RFID_APDU(String sendApdu);//cpu

	public native static int RFIDAuthenCard(byte nBlock, byte keyType,byte[] key);

	public static native int RFIDReadCard(byte nBlock, byte[] buf);

	public static native int RFIDWriteCard(byte nBlock, byte[] buf);

	public static native int RFIDInitValue(byte nBlock, int nMoney);

	public static native int RFIDInctValue(byte nBlock, int nMoney);

	public static native int RFIDDectValue(byte nBlock, int nMoney);

	public static native int RFIDRestor(byte nSrcBlock, byte nDesBlock);

	public static native int RFIDModuleOpen() ;
		
	

}
