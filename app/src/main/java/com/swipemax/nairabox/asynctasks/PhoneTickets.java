package com.swipemax.nairabox.asynctasks;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import com.swipemax.nairabox.AppController;
import com.swipemax.nairabox.R;
import com.swipemax.nairabox.Utils.Ticket;
import com.swipemax.nairabox.Utils.TicketClass;
import com.swipemax.nairabox.fragment.FragmentPhoneList;
import com.swipemax.nairabox.fragment.Fragment_Details;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by samwul on 17/09/2016.
 */
public class PhoneTickets extends AsyncTask<String, String, JSONObject> {
    Context context;
    FragmentActivity activity;
    ArrayList<Ticket> tickets;

    public PhoneTickets(FragmentActivity activity, Context context){
        this.activity = activity;
        this.context = context;
    }

    @Override
    public void onPreExecute(){
        tickets = AppController.getInstance().getMyTicketsByPhone();
    }

    @Override
    public JSONObject doInBackground(String... params) {
        // do apiCall to verify code
        String phone = params[0];
        System.out.println("code is - " + phone);

        String url = "https://revision.nairabox.com/v1/tickets/scanner.php?phone=" + phone;
        try {

            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            //add reuqest header
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            //con.setConnectTimeout(7000);

            int responseCode = con.getResponseCode();
            System.out.println(responseCode);

            if(responseCode != 200){ return null; }

            BufferedReader in = new BufferedReader( new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            String finalResponse = response.toString();

            System.out.println( "final response - " + finalResponse);

            JSONObject ret = new JSONObject(finalResponse);

            return ret;
        }
        catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onPostExecute(JSONObject result) {
        if (result != null) {
            try {
                int status = result.getInt("status");
                if (status == 200) {

                    JSONArray myTickets = result.getJSONArray("tickets");
                    if(myTickets.length() > 0){
                        for(int x = 0; x < myTickets.length(); x++){
                            JSONObject ticket = myTickets.getJSONObject(x);
                            Ticket t = new Ticket();
                            t.setCinema(result.getString("cinema"));
                            t.setDate(ticket.getString("date"));

                            TicketClass adult = new TicketClass();
                            adult.settClass("Adult");
                            adult.setQuantity(ticket.getInt("adults"));
                            adult.setPrice(result.getInt("adlt_price"));

                            TicketClass student = new TicketClass();
                            student.settClass("Student");
                            student.setQuantity(ticket.getInt("students"));
                            student.setPrice(result.getInt("sdnt_price"));

                            TicketClass children = new TicketClass();
                            children.settClass("Children");
                            children.setQuantity(ticket.getInt("children"));
                            children.setPrice(result.getInt("chld_price"));

                            ArrayList<TicketClass> tc = new ArrayList();
                            tc.add(adult); tc.add(student); tc.add(children);

                            t.setClasses(tc);

                            t.setMovie(result.getString("movie"));
                            t.setTime(ticket.getString("date"));
                            t.setTotalPaid(ticket.getInt("amount"));

                            tickets.add(t);
                        }

                        // launch FragmentPhoneList
                        FragmentPhoneList fpl = FragmentPhoneList.newInstance();
                        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
                        ft.setCustomAnimations(R.anim.slide_up, R.anim.slide_down);
                        ft.show(fpl);
                        ft.commit();
                    }else{
                        // you don't have any active tickets
                    }
                }
            } catch (JSONException e) {
                //
            }
        }
    }
}
