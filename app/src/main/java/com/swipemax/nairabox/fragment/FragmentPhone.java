package com.swipemax.nairabox.fragment;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.swipemax.nairabox.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnPhoneFragment} interface
 * to handle interaction events.
 * Use the {@link FragmentPhone#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentPhone extends DialogFragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    RelativeLayout btn_one, btn_two, btn_three, btn_four, btn_five, btn_six, btn_seven, btn_eight, btn_nine, btn_zero, btn_delete;
    View _view;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnPhoneFragment mListener;
    private TextView txt_number;
    private Button btn_accept;
    private Dialog d;
    private ImageView btn_back;

    public FragmentPhone() {
        // Required empty public constructor
    }
    @Override
    public void onStart() {
        super.onStart();
        d = getDialog();
        if (d != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            d.getWindow().setLayout(width, height);
            d.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        }

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentPhone.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentPhone newInstance(String param1, String param2) {
        FragmentPhone fragment = new FragmentPhone();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.MY_DIALOG);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        _view = inflater.inflate(R.layout.fragment_phone, container, false);
        setupButtons(_view);
        return _view;
    }

    // TODO: Rename method, update argument and hook method into UI event


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnPhoneFragment) {
            mListener = (OnPhoneFragment) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnPhoneFragment");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    void setupButtons(View v) {
        btn_one = (RelativeLayout) v.findViewById(R.id.btn_one);
        btn_one.setOnClickListener(this);
        btn_two = (RelativeLayout) v.findViewById(R.id.btn_two);
        btn_two.setOnClickListener(this);
        btn_three = (RelativeLayout) v.findViewById(R.id.btn_three);
        btn_three.setOnClickListener(this);
        btn_four = (RelativeLayout) v.findViewById(R.id.btn_four);
        btn_four.setOnClickListener(this);
        btn_five = (RelativeLayout) v.findViewById(R.id.btn_five);
        btn_five.setOnClickListener(this);
        btn_six = (RelativeLayout) v.findViewById(R.id.btn_six);
        btn_six.setOnClickListener(this);
        btn_seven = (RelativeLayout) v.findViewById(R.id.btn_seven);
        btn_seven.setOnClickListener(this);
        btn_eight = (RelativeLayout) v.findViewById(R.id.btn_eight);
        btn_eight.setOnClickListener(this);
        btn_nine = (RelativeLayout) v.findViewById(R.id.btn_nine);
        btn_nine.setOnClickListener(this);
        btn_zero = (RelativeLayout) v.findViewById(R.id.btn_zero);
        btn_zero.setOnClickListener(this);
        btn_delete = (RelativeLayout) v.findViewById(R.id.btn_delete);
        btn_delete.setOnClickListener(this);
        txt_number = (TextView) v.findViewById(R.id.txt_number);
        btn_accept = (Button) v.findViewById(R.id.btn_accept);
        btn_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txt_number.length()>0){
                    mListener.onPhoneEntered(txt_number.getText().toString());
                }
            }
        });
        btn_back = (ImageView) v.findViewById(R.id.img_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              dismiss();
            }
        });
    }

    @Override
    public void onClick(View view) {
        // Toast.makeText(getActivity(), "count "+ number_count, Toast.LENGTH_SHORT).show();
        //number_count=PinEntered.length();
        switch (view.getId()) {
            case R.id.btn_one:
                increaseNumber(1);
                break;
            case R.id.btn_two:
                increaseNumber(2);
                break;
            case R.id.btn_three:
                increaseNumber(3);
                break;
            case R.id.btn_four:
                increaseNumber(4);
                break;
            case R.id.btn_five:
                increaseNumber(5);
                break;
            case R.id.btn_six:
                increaseNumber(6);
                break;
            case R.id.btn_seven:
                increaseNumber(7);
                break;
            case R.id.btn_eight:
                increaseNumber(8);
                break;
            case R.id.btn_nine:
                increaseNumber(9);
                break;
            case R.id.btn_zero:
                increaseNumber(0);
                break;
            case R.id.btn_delete:
                decreaseNumber();
                break;
        }

    }

    private void increaseNumber(int number) {

        txt_number.setText(txt_number.getText().toString()+ number);
        _view.playSoundEffect(android.view.SoundEffectConstants.CLICK);
    }

    void  decreaseNumber(){

    String input_text= txt_number.getText().toString();
    if(input_text.length()>0) {
       txt_number.setText(input_text.substring(0, input_text.length() - 1));
    }
}
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnPhoneFragment {
        // TODO: Update argument type and name
        void onPhoneEntered(String phone);
    }

}
